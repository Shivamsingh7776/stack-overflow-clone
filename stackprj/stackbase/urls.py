from django.urls import path
from . import views

app_name = 'stackbase'

urlpatterns = [
    path('', views.home, name="home"),
    path('about/',views.about , name="question-lists"),
    
    # CRUD Function
    path('question/', views.QuestionListView.as_view(), name="question_list"),
    path('question/<int:pk>/', views.QuestionDetailView.as_view(), name="question_detail"),
]
