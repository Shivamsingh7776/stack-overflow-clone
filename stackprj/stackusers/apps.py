from django.apps import AppConfig


class StackusersConfig(AppConfig):
    defaul_auto_field
    name = 'stackusers'

    def ready(self):
        import stackusers.signals
