# Generated by Django 2.2 on 2022-12-30 12:56

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('stackusers', '0002_auto_20221230_1124'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='phone',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
